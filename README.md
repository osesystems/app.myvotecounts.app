# Reference application for OSES My Vote Counts mobile app
This repository of code has been created as a reference application demonstrating
how to build a fully cross-platform version of OSE Systems' My Vote Counts
mobile application.  This document should help people to test it in various
environments.

# Getting Started
First, ensure that you have the prerequisites installed.  The following
instructions are for developing on an Apple with [homebrew](https://brew.sh).
There's excellent instructions on installing ionic [here](https://ionicframework.com/docs/intro/cli).
Here's the quickstart:

1. You need `npm`
```bash
brew install node
```
1. You need `ionic/cli`
```bash
npm uninstall -g ionic
npm install -g @ionic/cli
```
1. You need to install the locally required packages:
```bash
npm install
```

Believe it or not, you're ready to view/use the app in your browser!
```bash
ionic serve --lab
```

# Compiling for Devices
First, add the platform for which you want to compile:

## iOS
For iOS, connect your phone to your computer, and run:
```bash
ionic cordova platform add ios@latest
ionic cordova prepare ios
open platforms/ios/*.xcworkspace
```
Xcode opens, and you can set a development team and run it on a local device or build for a store.

## Actually Getting into App Store
The build process for some of the Pods that we are using (example, the document scanner) have x86 architectures embedded in them.  These need to be removed.  Pending making this following step automatic (help anyone?) this cargo culted solution works:

https://stackoverflow.com/questions/30547283/submit-to-app-store-issues-unsupported-architecture-x86
```bash
echo "Target architectures: $ARCHS"

APP_PATH="${TARGET_BUILD_DIR}/${WRAPPER_NAME}"

find "$APP_PATH" -name '*.framework' -type d | while read -r FRAMEWORK
do
FRAMEWORK_EXECUTABLE_NAME=$(defaults read "$FRAMEWORK/Info.plist" CFBundleExecutable)
FRAMEWORK_EXECUTABLE_PATH="$FRAMEWORK/$FRAMEWORK_EXECUTABLE_NAME"
echo "Executable is $FRAMEWORK_EXECUTABLE_PATH"
echo $(lipo -info "$FRAMEWORK_EXECUTABLE_PATH")

FRAMEWORK_TMP_PATH="$FRAMEWORK_EXECUTABLE_PATH-tmp"

# remove simulator's archs if location is not simulator's directory
case "${TARGET_BUILD_DIR}" in
*"iphonesimulator")
    echo "No need to remove archs"
    ;;
*)
    if $(lipo "$FRAMEWORK_EXECUTABLE_PATH" -verify_arch "i386") ; then
    lipo -output "$FRAMEWORK_TMP_PATH" -remove "i386" "$FRAMEWORK_EXECUTABLE_PATH"
    echo "i386 architecture removed"
    rm "$FRAMEWORK_EXECUTABLE_PATH"
    mv "$FRAMEWORK_TMP_PATH" "$FRAMEWORK_EXECUTABLE_PATH"
    fi
    if $(lipo "$FRAMEWORK_EXECUTABLE_PATH" -verify_arch "x86_64") ; then
    lipo -output "$FRAMEWORK_TMP_PATH" -remove "x86_64" "$FRAMEWORK_EXECUTABLE_PATH"
    echo "x86_64 architecture removed"
    rm "$FRAMEWORK_EXECUTABLE_PATH"
    mv "$FRAMEWORK_TMP_PATH" "$FRAMEWORK_EXECUTABLE_PATH"
    fi
    ;;
esac

echo "Completed for executable $FRAMEWORK_EXECUTABLE_PATH"
echo $(lipo -info "$FRAMEWORK_EXECUTABLE_PATH")

done
```

## Android
For Android, connect your phone to your computer, and run:
```bash
ionic cordova platform add android@latest
ionic cordova prepare android
ionic cordova run android --device
```

To build a distributable APK:
```bash
ionic cordova build android --prod --release
```

To build for the Play Store:
```bash
./build-for-android-store.sh
```

### The Play Store Hates Me

Sometimes, the Play Store tells you your application isn't good enough because it's targeting the wrong API version.  This requires adjusting the `targetApiVersion` in `config.xml`,
```xml
    <platform name="android">
      ...
       <preference name="android-minSdkVersion" value="21" />
       <preference name="android-targetSdkVersion" value="29" />
```
 and ensuring that you have the latest version of `cordova-android` installed:
```bash
ionic cordova platform rm android
ionic cordova platform add android@latest
```

# Playing with the App
There are a few "hidden" developer features in the application to make working with it easier.  To turn on the developer menu, tap the logo from the Welcome screen.  (You can always get back to the Welcome screen by touching the version information in the side menu.)

##  Changing the API server
From the developer menu, select "Change API Server".  Some reasonable examples include [https://api.myvotecounts.com] the production server, [https://api-staging.myvotecounts.com] the development server, and [http://mymachine.local:3000] perhaps a server running locally on your mac that you started with `./bin/rails s -b 0.0.0.0`.

