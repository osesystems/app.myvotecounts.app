import { Component, Input, OnInit } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { StateDataService } from "../services/state-data.service";

@Component({
  selector: 'event-timeline',
  templateUrl: './event-timeline.component.html',
  styleUrls: ['./event-timeline.component.scss'],
})

export class EventTimelineComponent implements OnInit {
  @Input() statename:string = this.statedata.longName(this.storage.voting_state);
  data:any = this.statename ? this.statedata.getStateData(this.statename) : {};
  keys:any = Object.keys(this.data);
  timeline:any = this.data["timelineData"];
  events:any = [];

  constructor(private storage: StorageService, private statedata: StateDataService) { }
  ngOnInit() { this.extract_events_from_timeline(this.timeline); }

  extract_events_from_timeline(timeline) {
    let events = [];
    let now = new Date();
    let tzoff = now.getTimezoneOffset() * 60000;

    if (timeline) {
      for (var i = 0; i < timeline.length; i++) {
	let event = { "what": "" };
	let due_date = timeline[i].date.replace(".000Z", "");

	/* Okay, so THEN is the start of the day on which this event is due.
	   Which means it is due by the end of that day, if there's no time
	   component specified in the date given. */
	due_date = due_date.replace("T00:00:00", "T11:59:59");

	let then = new Date(due_date);

	if (then < now) continue;

	let timediff = then.getTime() - now.getTime(); 
	let daydiff = Math.floor(timediff / (1000 * 3600 * 24));
	let when = daydiff + " days";
	let text_date = then.toLocaleDateString('en', { month: 'short', day: 'numeric' });

	if (daydiff < 1) {
	  when = "Today";
	  text_date = "today";
	} else if (daydiff == 1) {
	  when = "1 day";
	  text_date = "tomorrow";
	}
	event["what"] = timeline[i].text.replace("[DATE]", text_date) 
	event["when"] = when;
	events.push(event);
      }
    }
    this.events = events;
  }
}
