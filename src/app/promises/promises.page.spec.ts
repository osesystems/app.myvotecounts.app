import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromisesPage } from './promises.page';

describe('PromisesPage', () => {
  let component: PromisesPage;
  let fixture: ComponentFixture<PromisesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromisesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromisesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
