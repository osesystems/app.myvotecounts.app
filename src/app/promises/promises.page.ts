import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, AlertController, IonSlides } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-promises',
  templateUrl: './promises.page.html',
  styleUrls: ['./promises.page.scss'],
})

export class PromisesPage implements OnInit {
  @ViewChild('slides', { static: false })
  private readonly slides!: IonSlides;
  public slideOpts = { touchRatio: 0 };
  public options = ["Parent", "Child", "Sibling", "Significant Other", "Friend/Partner", "Other"];
  public people:any = {};
  public any_people:boolean = false;
  private shared:boolean = false;
  private to_whom:string = "Not Gathered";

  constructor(
    private nav:NavController,
    public api:ApiService,
    private social:SocialSharing
  ) { }

  ngOnInit() { }
  ionViewDidEnter() { }

  updateToWhom() {
    let collection = Object.keys(this.people);
    this.to_whom = Object.keys(this.people).join(",");
    return this.to_whom;
  }
  
  public add_remove_person(option) {
    let present = this.people[option];

    if (!present)
      this.people[option] = true;
    else
      delete this.people[option];

    if (Object.keys(this.people).length > 0)
      this.any_people = true;
    else
      this.any_people = false;
  }

  public click_checkbox(idname) {
    let cb = document.getElementById(idname);
    if (cb) cb.click();
  }

  next() { this.slides.slideNext(); }
  do_share() { this.shared = true; this.show_share(); }
  go_home() {
    this.api.addPromise(this.updateToWhom(), this.shared).subscribe((res) => { console.log("PROMISE:", res); });
    this.nav.navigateBack(["/home"]);
  }
  
  show_share() {
    // this is the complete list of currently supported params you can pass to the plugin (all optional)
    let short_message = `I promised to vote!`;
    var options = {
      message: short_message,		 // not supported on some apps (Facebook, Instagram)
      subject: short_message,		 // fi. for email
      // files: ['', ''],		 // an array of filenames either locally or remotely
      url: 'https://myvotecounts.com/promise.html',
      chooserTitle: 'Pick an app'	 // Android only, you can override the default share sheet title
    };
    this.social.shareWithOptions(options);
  }
}

