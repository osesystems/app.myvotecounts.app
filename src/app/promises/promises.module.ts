import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SharedModule } from '../modules/shared.module';
import { PromisesPageRoutingModule } from './promises-routing.module';
import { PromisesPage } from './promises.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    PromisesPageRoutingModule
  ],
  declarations: [PromisesPage]
})
export class PromisesPageModule {}
