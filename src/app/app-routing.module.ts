import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'state',
    loadChildren: () => import('./state/state.module').then( m => m.StatePageModule)
  },
  {
    path: 'notifications-permission',
    loadChildren: () => import('./notifications-permission/notifications-permission.module').then( m => m.NotificationsPermissionPageModule)
  },
  {
    path: 'how-and-where',
    loadChildren: () => import('./how-and-where/how-and-where.module').then( m => m.HowAndWherePageModule)
  },
  {
    path: 'scan-ballot',
    loadChildren: () => import('./scan-ballot/scan-ballot.module').then( m => m.ScanBallotPageModule)
  },
  {
    path: 'voted',
    loadChildren: () => import('./voted/voted.module').then( m => m.VotedPageModule)
  },
  {
    path: 'prevented',
    loadChildren: () => import('./prevented/prevented.module').then( m => m.PreventedPageModule)
  },
  {
    path: 'promises',
    loadChildren: () => import('./promises/promises.module').then( m => m.PromisesPageModule)
  },
  {
    path: 'privacy',
    loadChildren: () => import('./privacy/privacy.module').then( m => m.PrivacyPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
