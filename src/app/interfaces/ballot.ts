// ballot.ts: -*- typescript -*-  DESCRIPTIVE TEXT.
// 
//  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
//  Author: Brian J. Fox (bfox@opuslogica.com)
//  Birthdate: Fri Jun 26 13:24:28 2020.

export interface Selection {
  id?:any;
  contest_id?:any;
  name?:string;
  rank?:number;
}

export interface Contest {
  id?:any;
  ballot_id?:any;
  name?:string;
  type?:string;
  selections?:Selection[];
}

export interface Ballot {
  id?:any;
  imaged_at?:number;
  image?:string;
  created_at?:number;
  updated_at?:number;
  is_mail_in?:boolean;
  contests?:Contest[];
}


