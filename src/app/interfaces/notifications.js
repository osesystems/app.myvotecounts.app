//notifications.js: -*- JavaScript-IDE -*-  DESCRIPTIVE TEXT.
//
// Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
// Author: Brian J. Fox (bfox@opuslogica.com)
// Birthdate: Fri Oct 16 11:21:25 2020.
export interface APSAlert {
  title?:string;
  body?:string;
}

export interface PushNotificationAPS {
  alert?: APSAlert;
  sound?:string;
};

export interface PushNotification {
  aps?:PushNotificationAPS;
  message?:string;
}

