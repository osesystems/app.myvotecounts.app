import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { Device } from '@ionic-native/device/ngx';
import { File } from '@ionic-native/file/ngx';

import { AppVersion } from '@ionic-native/app-version/ngx';
import { PipesModule } from './pipes/pipes.module';
import { ApiService } from './services/api.service';
import { StorageService } from './services/storage.service';
import { MenuComponent } from './menu/menu.component';

import { FormsModule } from '@angular/forms';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';

@NgModule({
  declarations: [AppComponent, MenuComponent],
  entryComponents: [],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule, PipesModule, IonicModule.forRoot(), ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiService,
    StorageService,
    NativeStorage,
    AppVersion,
    SocialSharing,
    OpenNativeSettings,
    InAppBrowser,
    Diagnostic,
    Device,
    File,
    HTTP,
    FirebaseX,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
