import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NavController, AlertController } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';
import { ApiService } from '../services/api.service';
import { Platform, ToastController } from '@ionic/angular';
import { Selection, Contest, Ballot } from '../interfaces/ballot';
import { FakeData } from '../interfaces/fake_data';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { File } from '@ionic-native/file/ngx';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-scan-ballot',
  templateUrl: './scan-ballot.page.html',
  styleUrls: ['./scan-ballot.page.scss']
})
export class ScanBallotPage implements OnInit {

  ballot:Ballot = null;
  scans:any[] = [];
  
  constructor(
    private http:HttpClient,
    private zone:NgZone,
    private api:ApiService,
    private router:Router,
    private nav:NavController,
    private activatedRoute:ActivatedRoute,
    private platform:Platform,
    private webview: WebView,
    public  device: Device,
    private asc: ActionSheetController,
    private toaster:ToastController
  ) { }

  ngOnInit() { }

  scan_one(event=null) {
    if (event) event.stopPropagation();
    this.platform.ready().then((ready) => {
      if (window["scan"]) {
	this.scan_ballot();
      } else {
	this.scan_worked("/assets/image/sample-ballot.jpg");
      }
    });
  }

  /* ScanDoc plugin documentation:
     "sourceType" will by default take value 1 if no value is set | 0 for gallery | 1 for camera. 
     "fileName" will take default value "image" if no value set. Supported only on 4.x.x plugin version.
     "quality" will take default value 1.0 (highest). Lowest value is 5.0. Any value in between will be accepted.
     "returnBase64" will take default boolean value false, meaning image URL is returned. If true base64 is returned. */
  scan_ballot(supplied_filename=null) {
    if (supplied_filename) {
      this.scan_worked(supplied_filename);
    } else {
      this.platform.ready().then((ready) => {
	var options = {sourceType: 1, quality: 1.0, fileName: `ballot-${(new Date()).getTime()}`, returnBase64: false };
	window["scan"].scanDoc((x) => this.zone.run(() => { this.scan_worked(x); }), (e) => this.zone.run(() => { this.scan_failed(e) }), options);
      });
    }
  }

  scan_worked(fname:string) {
    let filename = fname;
    let src;
    
    try {
      src = this.webview.convertFileSrc(filename);
    } catch(e) {
      console.log(`convertFileSrc(${filename}) failed: (${e}`); src = fname;
    };
    console.log(`SRC: ${src}`);
    let scan = { filename: fname, src: src, status: "Scanned" };

    /* If there is an empty slot, we are filling it with this one. */
    let empty = 0;
    for (empty = 0; empty < this.scans.length; empty++) if (this.scans[empty] == undefined) break;

    if (empty == this.scans.length) {
      this.scans.push(scan);
    } else {
      this.scans[empty] = scan;
    }
    this.zone.run(() => { setTimeout(() => { this.upload(scan); }, 4000); });
  }
  
  scan_failed(message:string) {
    this.error(message);
  }

  done() {
    this.nav.navigateBack(["/home"]);
  }

  upload(scan) {
    this.getBase64(scan, () => { this.do_upload(scan); });
  }

  do_upload(scan) {
    this.zone.run(() => { scan.status = "Uploading"; });
    this.api.post('ballots', { image: scan.image_data, imaged_at: new Date() })
      .subscribe((response) => {
	if (response && response["result"] && response["result"]["ballot"]) {
	  scan.ballot = <Ballot>response["result"]["ballot"];
	  this.zone.run(() => { scan.status = "Uploaded"; });
	} else {
	  this.zone.run(() => { scan.status = "Upload Failed"; });
	}
      });
  }

  getBase64(scan, next) {
    let with_file:Function = (file) => {
      let reader = new FileReader();
      reader.onload = () => { scan.image_data = reader.result; if (next) next(); };
      reader.onerror = (error) => { console.log('Error: ', error); };
      reader.readAsDataURL(file);
    }

    let resolveLocalFileSystemURL:Function = window["resolveLocalFileSystemURL"];
    if (scan.filename[0] != "/") {
      resolveLocalFileSystemURL(
	scan.filename,
	(entry) => { entry.file(with_file, (e) => { console.log("ERROR: ", e); }); },
	(error) => { this.error(`Error accessing file ${scan.filename}`); }
      );
    } else {
      with_file(scan.filename);
    }
  }

  async pageClicked(idx) {
    const actionSheet = await this.asc.create({
      header: 'Scan',
      cssClass: 'scan-action-sheet',
      buttons: [{
        text: 'Remove This Page',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
	  this.scans.splice(idx, 1);
        }
      }, {
        text: 'Replace This Page',
        icon: 'document',
        handler: () => {
	  this.scans[idx] = undefined;
	  this.scan_one();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  
  async inform(message:string) {
    let t = await this.toaster.create({ message: message, position: 'top', duration: 3000 });
    t.present(); 
  }

  async error(message:string) {
    let t = await this.toaster.create({ message: message, position: 'top', duration: 3000 });
    t.present(); 
  }
}
