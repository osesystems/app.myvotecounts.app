import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScanBallotPage } from './scan-ballot.page';

const routes: Routes = [
  {
    path: '',
    component: ScanBallotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScanBallotPageRoutingModule {}
