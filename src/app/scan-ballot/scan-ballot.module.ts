import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WebView } from '@ionic-native/ionic-webview/ngx';

import { IonicModule } from '@ionic/angular';
import { PipesModule } from '../pipes/pipes.module';

import { ScanBallotPageRoutingModule } from './scan-ballot-routing.module';
import { ScanBallotPage } from './scan-ballot.page';
import { SharedModule } from '../modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    ScanBallotPageRoutingModule,
    SharedModule,
  ],
  providers: [ WebView ],
  declarations: [ ScanBallotPage ],
})
export class ScanBallotPageModule {}
