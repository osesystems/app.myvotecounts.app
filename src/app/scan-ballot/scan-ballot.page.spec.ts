import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ScanBallotPage } from './scan-ballot.page';

describe('ScanBallotPage', () => {
  let component: ScanBallotPage;
  let fixture: ComponentFixture<ScanBallotPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScanBallotPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ScanBallotPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
