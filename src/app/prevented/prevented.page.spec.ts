import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PreventedPage } from './prevented.page';

describe('PreventedPage', () => {
  let component: PreventedPage;
  let fixture: ComponentFixture<PreventedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreventedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PreventedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
