import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PreventedPageRoutingModule } from './prevented-routing.module';
import { PreventedPage } from './prevented.page';
import { SharedModule } from '../modules/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreventedPageRoutingModule,
    SharedModule
  ],
  declarations: [PreventedPage]
})
export class PreventedPageModule {}
