import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, AlertController, IonSlides } from '@ionic/angular';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-prevented',
  templateUrl: './prevented.page.html',
  styleUrls: ['./prevented.page.scss'],
})

export class PreventedPage implements OnInit {
  @ViewChild('slides', { static: false })
  public slides: IonSlides;
  public readonly slideOpts: Readonly<any> = { touchRatio: 0 };
  
  public readonly reasons = [
    "I had to work",
    "I had to care for my child/parent/dependent",
    "I felt intimidated",
    "I waited too long in line",
    "The polling place couldn't accomodate me",
    "I don't want to get COVID-19",
    "Reason not listed"
  ];
  public d:any = { by_mail: false, in_person: false, other: false, contact_me: false };
  public prevented_reasons = {};
  public any_reasons:boolean = false;
  public more_details = "";
  
  constructor(
    private nav:NavController,
    public api:ApiService
  ) { }
  
  ngOnInit() {
    this.prevented_reasons = [];
  }

  public add_remove_reason(reason) {
    let present = this.prevented_reasons[reason];

    if (!present)
      this.prevented_reasons[reason] = true;
    else
      delete this.prevented_reasons[reason];

    if (Object.keys(this.prevented_reasons).length > 0)
      this.any_reasons = true;
    else
      this.any_reasons = false;
  }

  public click_checkbox(idname) {
    let cb = document.getElementById(idname);
    if (cb) cb.click();
  }
  
  public next() { this.slides.slideNext(); }
  public prev() { this.slides.slidePrev(); }

  private method() {
    let result = [];
    if (this.d.by_mail) result.push("by mail ");
    if (this.d.in_person) result.push("in_person ");
    if (this.d.other) result.push("other ");
    return result.join();
  }

  public finish() {
    let experience = {
      outcome: "prevented",
      method: this.method(),
      reasons: Object.keys(this.prevented_reasons),
      description: this.more_details,
      contact_me: this.d.contact_me
    };

    this.api.updateExperience(experience).subscribe((response) => { console.log(response); });
    this.slides.slideNext();
  }

  public go_home() {
    this.nav.navigateBack(["/home"]);
  }
}
