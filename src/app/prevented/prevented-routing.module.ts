import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreventedPage } from './prevented.page';

const routes: Routes = [
  {
    path: '',
    component: PreventedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreventedPageRoutingModule {}
