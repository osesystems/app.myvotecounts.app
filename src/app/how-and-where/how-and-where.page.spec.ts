import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HowAndWherePage } from './how-and-where.page';

describe('HowAndWherePage', () => {
  let component: HowAndWherePage;
  let fixture: ComponentFixture<HowAndWherePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowAndWherePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HowAndWherePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
