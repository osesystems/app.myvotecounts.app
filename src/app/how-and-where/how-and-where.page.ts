import { Component, Input, OnInit } from '@angular/core';
import { StateDataService } from "../services/state-data.service";
import { StorageService } from "../services/storage.service";


@Component({
  selector: 'app-how-and-where',
  templateUrl: './how-and-where.page.html',
  styleUrls: ['./how-and-where.page.scss'],
})
export class HowAndWherePage implements OnInit {
  @Input() statename:string = this.statedata.longName(this.storage.voting_state);
  data:any = this.statedata.getStateData(this.statename);
  keys:any = Object.keys(this.data);
  
  constructor(
    private statedata: StateDataService,
    private storage: StorageService,
  ) {}

  ngOnInit() { }
}
