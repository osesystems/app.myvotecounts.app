import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../modules/shared.module';
import { PipesModule } from '../pipes/pipes.module';
import { HowAndWherePageRoutingModule } from './how-and-where-routing.module';
import { HowAndWherePage } from './how-and-where.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    PipesModule,
    HowAndWherePageRoutingModule
  ],
  declarations: [HowAndWherePage]
})
export class HowAndWherePageModule {}
