import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HowAndWherePage } from './how-and-where.page';

const routes: Routes = [
  {
    path: '',
    component: HowAndWherePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HowAndWherePageRoutingModule {}
