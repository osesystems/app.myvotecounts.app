import { Component, Input, OnInit } from '@angular/core';
import { StateDataService } from "../services/state-data.service";
import { StorageService } from "../services/storage.service";
import { NavController, IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() back_arrow:boolean = true;
  @Input() show_menu:boolean = false;
  @Input() statename:string = null;
  @Input() back: Function;
  @Input() slides: IonSlides = null;
  
  constructor(
    private statedata: StateDataService,
    private storage: StorageService,
    private nav: NavController
  ) {}

  ngOnInit() {
    this.statename = this.statedata.longName(this.storage.voting_state);
  }

  ionViewDidEnter() {
    this.statename = this.statedata.longName(this.storage.voting_state);
  }

  choose_state() {
    this.nav.navigateBack(["/state"]);
  }

  go_back() {
    if (this.back) {
      this.back();
    } else if (this.slides) {
      this.slides.slidePrev();
    } else {
      this.nav.back();
    }
  }
}
