import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { StorageService } from '../services/storage.service';
import { Device } from '@ionic-native/device/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  constructor(
    private nav: NavController,
    public device: Device,
    private storage: StorageService
  ) {}
  
  ngOnInit() {}

  ionViewDidEnter() {
  }

  go_page(name, must_have_state=false) {
    let state = this.storage.voting_state;
    if (must_have_state && !state) {
      alert("We can't help you with this without knowing what state you want to do it for.  Please select one by touching \"Select State\" at the top of the screen.");
    } else {
      this.nav.navigateForward([name]);
    }
  }
}
