import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { StorageService } from '../services/storage.service';
import { ApiService } from '../services/api.service';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  is_developer:boolean = this.storage.is_developer;
  button_message = "Let's Get Started";

  constructor(
    private readonly nav:NavController,
    private storage:StorageService,
    private api:ApiService
) { }

  ngOnInit() {
    if (this.storage.welcomed) {
      this.button_message = "Let's Continue";
    }
    this.storage.welcomed = true;
  }

  go_home() {
    if (!this.storage.voting_state)
      this.nav.navigateForward(["/state"]);
    else
      this.nav.navigateForward(["/home"]);
  }

  toggle_developer_mode() {
    this.is_developer = !this.is_developer;
    this.api.developerMode = this.is_developer;
  }
}
