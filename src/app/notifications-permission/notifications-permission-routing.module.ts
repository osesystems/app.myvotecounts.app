import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotificationsPermissionPage } from './notifications-permission.page';

const routes: Routes = [
  {
    path: '',
    component: NotificationsPermissionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationsPermissionPageRoutingModule {}
