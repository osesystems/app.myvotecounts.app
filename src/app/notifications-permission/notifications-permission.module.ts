import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../modules/shared.module';

import { NotificationsPermissionPageRoutingModule } from './notifications-permission-routing.module';

import { NotificationsPermissionPage } from './notifications-permission.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    NotificationsPermissionPageRoutingModule
  ],
  declarations: [NotificationsPermissionPage]
})
export class NotificationsPermissionPageModule {}
