import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { StorageService } from '../services/storage.service';
import { NotificationsService } from '../services/notifications.service';

@Component({
  selector: 'app-notifications-permission',
  templateUrl: './notifications-permission.page.html',
  styleUrls: ['./notifications-permission.page.scss'],
})
export class NotificationsPermissionPage implements OnInit {

  constructor(
    private nav: NavController,
    private storage: StorageService,
    private notifications: NotificationsService
  ) { }
  ngOnInit() { }

  /* If the user has already given us permission, move on immediately */
  ionViewWillEnter() {
  }

  /* The user has declined to get push notiications from us. */
  skip() {
    void this.nav.navigateForward(['/home']);
  }

  /* Ask the user for reals about permission. */
  next() {
    this.notifications.getPermission();
    this.nav.navigateForward(['/home']);
  }
}
