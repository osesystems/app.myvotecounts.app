import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../modules/shared.module';
import { StatePageRoutingModule } from './state-routing.module';
import { StatePage } from './state.page';
import { StateChooserComponent } from "../state-chooser/state-chooser.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    StatePageRoutingModule
  ],
  declarations: [StatePage, StateChooserComponent]
})
export class StatePageModule {}
