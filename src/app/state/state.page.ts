import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { StorageService } from '../services/storage.service';
import { NotificationsService } from '../services/notifications.service';

@Component({
  selector: 'app-state',
  templateUrl: './state.page.html',
  styleUrls: ['./state.page.scss'],
})
export class StatePage implements OnInit {
  current:string = this.storage.voting_state;

  disabled:boolean = true;

  @Input()
  skip_allowed:boolean = (!this.current || this.current.length === 0);
  
  constructor(
    private nav: NavController,
    private notifications: NotificationsService,
    private storage: StorageService
  ) { }

  ngOnInit() { }

  skip() { this.nav.navigateForward(["/home"]); }
  next() {
    if (!!window["cordova"]) {
      this.notifications.hasPermission().then((perm) => {
	if (perm) {
	  /* We have permission already. Just move on. */
	  this.nav.navigateForward(["/home"]);
	} else {
	  this.nav.navigateForward(["/notifications-permission"]); }
      });
    } else {
      this.nav.navigateForward(["/home"]);
    }
  }

  check_disabled() {
    this.current = this.storage.voting_state;
    return (!this.current || this.current.length == 0);
  }
}
