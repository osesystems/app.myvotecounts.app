import { Component, OnInit } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { StorageService } from '../services/storage.service';
import { NavController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Device } from '@ionic-native/device/ngx';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  browser:any;
  is_developer:boolean = this.api.developerMode;
  app_name:string = null;
  app_version:string = "<can't tell>";
  
  constructor(
    private storage: StorageService,
    private api: ApiService,
    private nav: NavController,
    private platform: Platform,
    private iab: InAppBrowser,
    private diag: Diagnostic,
    private toaster: ToastController,
    private appVersion: AppVersion,
    private device: Device
  ) {
    this.api.developerModeChanged.subscribe((new_mode) => { this.is_developer = new_mode; });
    this.platform.ready().then((ready) => {
      appVersion.getAppName().then(
	(name) => { this.app_name = name; },
	(error) => { this.app_name = 'My Vote Counts'; }
      );
      appVersion.getVersionNumber().then(
	(version) => { this.app_version = version; },
	(error) => { this.app_version = '0.0'; }
      );
    });
  }

  ngOnInit() {
  }
  
  go_about() {
    let plat = this.device.platform;

    if (plat) {
      plat = plat.toLowerCase();
    } else {
      plat = "ios";
    }
    
    this.browser = this.iab.create(`https://myvotecounts.com/aboutus-${plat}.html`, '_blank', 'location=no');
  }

  go_privacy() { this.nav.navigateRoot(["/privacy"]); }

  go_settings() {
    this.platform.ready().then(() => {
      this.diag.switchToSettings().then((val) => {
        console.log(val);
      });
    });
  }

  turn_off_developer_mode() {
    this.api.developerMode = false;
  }

  clear_local_storage() {
    this.storage.clearData();
  }

  change_api_server() {
    let server = window.prompt("Enter API Server:");
    if (server && server.length > 0) this.api.set_server(server);
  }

  jump_to_page(page=null) {
    if (page) {
      this.nav.navigateRoot([page]);
    } else {
      page = window.prompt("Enter Page Name:");
      if (page && page.length > 0)
	this.nav.navigateRoot([page]);
    }
  }

  async inform(message:string) {
    let t = await this.toaster.create({ message: message, position: 'top', duration: 3000 });
    t.present(); 
  }
}
