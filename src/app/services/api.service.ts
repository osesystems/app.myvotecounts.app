/* Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
   Author: Brian J. Fox (bfox@opuslogica.com)
   Birthdate: Sat Sep 26 09:15:32 2020.
*/
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable, NgZone, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, Platform } from '@ionic/angular';
import { RouterDirection } from '@ionic/core';
import { from, Observable, of } from 'rxjs';
import { catchError, map, shareReplay, switchMap, tap } from 'rxjs/operators';
import { Device } from '@ionic-native/device/ngx';

import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public developerModeChanged = new EventEmitter();


  public api_local: string = 'http://localhost:3000';
  public api_silver: string = 'http://silverback.local:3000';
  public api_stage: string = 'https://api-staging.myvotecounts.app';
  public api_prod: string = 'https://api.myvotecounts.app';

  public api_server: string = this.api_prod;
  public api_base: string = `${this.api_server}/api/v1`;

  public deviceID: string | undefined = undefined;

  public support: { urls: any } = {
    urls: {
      terms: 'https://www.myvotecounts.app/terms.html',
      privacy: 'https://www.myvotecounts.app/privacy.html',
    },
  };

  public caches$: { [endpoint: string]: Observable<unknown> | undefined } = {};

  public constructor(
    private readonly ngZone: NgZone,
    private readonly http: HttpClient,
    private readonly platform: Platform,
    private readonly storage: StorageService,
    private readonly nav: NavController,
    private readonly router: Router,
    private readonly device: Device,
  ) {
    this.deviceID = this.storage.deviceID;
    this.set_server(this.storage.api_server);
  }

  get developerMode() {
    return this.storage.is_developer;
  }

  set developerMode(mode) {
    this.storage.is_developer = mode ? true : false;
    this.developerModeChanged.emit(mode);
  }

  private g_handleError<T>(
    operation: string = 'operation',
    result: T,
  ): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {
      console.error(`${operation} failed: ${error ? error.message : ''}`);

      return of(result);
    };
  }

  public set_server(baseurl: string | undefined): void {
    let server = baseurl !== undefined ? baseurl : this.api_prod;
    server = server.toLowerCase().trim();

    if (!server.startsWith('http')) {
      server = `https://${server}`;
    }
    if (server.includes('local')) {
      server = server.replace('https', 'http');
    }

    this.api_server = server;
    this.storage.api_server = this.api_server;
    this.api_base = `${this.api_server}/api/v1`;
    this.update_ancillary_urls();
  }

  /* In case changing the API server causes privacy_url to change, for example. */
  public update_ancillary_urls(): void {}

  public setDeviceID(device_id: string): void {
    this.deviceID = device_id;
    this.storage.deviceID = this.deviceID;
  }

  public apiurl(endpoint: string): string {
    return `${this.api_base}/${endpoint}`;
  }

  public synchronous_post<T, D>(endpoint: string, data?: D): T | undefined {
    const request = new XMLHttpRequest();
    const uri = this.apiurl(endpoint);
    request.open('POST', uri, false);
    request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

    if (this.deviceID !== undefined) { request.setRequestHeader('deviceID', this.deviceID); }
    if (this.storage.voting_state) { request.setRequestHeader('voting-state', this.storage.voting_state); }

    if (data !== undefined) {
      request.send(JSON.stringify(data));
    } else {
      request.send(undefined);
    }

    if (request.status >= 200 && request.status < 300) {
      return JSON.parse(request.response);
    }

    return undefined;
  }

  public initialize(): void {
    void this.platform.ready().then(() => {
      if (!this.deviceID || this.deviceID.length === 0) {
	this.setDeviceID(this.device.uuid || 'developer-device.uuid');
        if (window.location.pathname !== '/welcome') {
          void this.nav.navigateRoot(['/welcome']);
        }
      } else {
        const route: string = this.storage.route;
        if (route !== undefined && route.length > 0) {
          if (window.location.pathname.length < 2) {
            this.nav.navigateRoot([route]);
          }
        }
      }
    });
  }

  private handleError<T>(operation='operation', result?: T) {
    return (error:any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error ? error.message : ""}`);
      return of(result as T);
    };
  }
  
  /* Generic POST. */
  post(endpoint, data=null) {
    let headers = new HttpHeaders();
    headers = headers.set('deviceID', this.deviceID || "developer-device.uuid");

    if (this.storage.voting_state) { headers = headers.set('voting-state', this.storage.voting_state); }
    return this.http.post(this.apiurl(endpoint), data, { headers: headers })
      .pipe(catchError(this.handleError(endpoint)));
  }

  /* Generic GET. */
  get(endpoint, data=null) {
    let headers = new HttpHeaders();
    let options = { };
    headers = headers.set('deviceID', this.deviceID || "developer-unique-id");
    if (this.storage.voting_state) { headers = headers.set('voting-state', this.storage.voting_state); }
    if (data) options["params"] = new HttpParams(data);
    options["headers"] = headers;
    return this.http.get<any>(this.apiurl(endpoint), options).pipe(catchError(this.handleError(endpoint)));
  }

  /* Generic cached GET. */
  cached_get(endpoint:string, data:any=null, force?:boolean) {
    let headers = new HttpHeaders();
    let options = { };
    headers = headers.set('deviceID', this.deviceID);
    if (this.storage.voting_state) { headers = headers.set('voting-state', this.storage.voting_state); }
    if (data) options["params"] = new HttpParams(data);
    options["headers"] = headers;

    if (force || !this.caches$[endpoint]) this.caches$[endpoint] = null;
    if (!this.caches$[endpoint]) {
      this.caches$[endpoint] = this.http.get<any>(this.apiurl(endpoint), options)
	.pipe(
	  tap((x) => { shareReplay(1); }),
	  catchError((err) => { console.error("api.cached_get(" + endpoint + ")", err); return of("api.cached_get"); })
	);
    }
    return this.caches$[endpoint];
  }

  /* savePushToken. */
  public savePushToken(token: string, token_type: string): Observable<any> {
    token_type = token_type || 'firebase';
    this.storage.push_token = token;
    this.storage.push_token_type = token_type;
    return this.post('tokens', { token: token, type: token_type });
  }

  /* updateExperience. */
  public updateExperience(experience:any): Observable<any> {
    return this.post('experiences', experience);
  }

  /* addPromise. */
  public addPromise(to_whom:string, shared:boolean): Observable<any> {
    return this.post('promises', { to_whom: to_whom, shared: shared });
  }

  public clearData(): Observable<any> {
    let headers = new HttpHeaders();
    let options = { };
    headers = headers.set('deviceID', this.deviceID || "developer-unique-id");
    options["headers"] = headers;
    return this.http.delete<any>(this.apiurl("my-data"), options).pipe(catchError(this.handleError("my-data")));
  }
}
