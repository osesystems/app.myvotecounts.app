// state-data.service.ts: -*- typescript -*-  DESCRIPTIVE TEXT.
// 
//  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
//  Author: Brian J. Fox (bfox@opuslogica.com)
//  Birthdate: Sun Sep 27 07:01:06 2020.
import { Injectable, NgZone } from '@angular/core';
import * as data from './data/StateData.json';

@Injectable({
  providedIn: 'root',
})

export class StateDataService {

  public stateData = (data as any).default;
  
  public stateInfo = [
    { code2: "AL", name: "Alabama" },
    { code2: "AK", name: "Alaska" },
    { code2: "AZ", name: "Arizona" },
    { code2: "AR", name: "Arkansas" },
    { code2: "CA", name: "California" },
    { code2: "CO", name: "Colorado" },
    { code2: "CT", name: "Connecticut" },
    { code2: "DE", name: "Delaware" },
    { code2: "DC", name: "District Of Columbia" },
    { code2: "FL", name: "Florida" },
    { code2: "GA", name: "Georgia" },
    { code2: "HI", name: "Hawaii" },
    { code2: "ID", name: "Idaho" },
    { code2: "IL", name: "Illinois" },
    { code2: "IN", name: "Indiana" },
    { code2: "IA", name: "Iowa" },
    { code2: "KS", name: "Kansas" },
    { code2: "KY", name: "Kentucky" },
    { code2: "LA", name: "Louisiana" },
    { code2: "ME", name: "Maine" },
    { code2: "MD", name: "Maryland" },
    { code2: "MA", name: "Massachusetts" },
    { code2: "MI", name: "Michigan" },
    { code2: "MN", name: "Minnesota" },
    { code2: "MS", name: "Mississippi" },
    { code2: "MO", name: "Missouri" },
    { code2: "MT", name: "Montana" },
    { code2: "NE", name: "Nebraska" },
    { code2: "NV", name: "Nevada" },
    { code2: "NH", name: "New Hampshire" },
    { code2: "NJ", name: "New Jersey" },
    { code2: "NM", name: "New Mexico" },
    { code2: "NY", name: "New York" },
    { code2: "NC", name: "North Carolina" },
    { code2: "ND", name: "North Dakota" },
    { code2: "OH", name: "Ohio" },
    { code2: "OK", name: "Oklahoma" },
    { code2: "OR", name: "Oregon" },
    { code2: "PA", name: "Pennsylvania" },
    { code2: "RI", name: "Rhode Island" },
    { code2: "SC", name: "South Carolina" },
    { code2: "SD", name: "South Dakota" },
    { code2: "TN", name: "Tennessee" },
    { code2: "TX", name: "Texas" },
    { code2: "UT", name: "Utah" },
    { code2: "VT", name: "Vermont" },
    { code2: "VA", name: "Virginia" },
    { code2: "WA", name: "Washington" },
    { code2: "WV", name: "West Virginia" },
    { code2: "WI", name: "Wisconsin" },
    { code2: "WY", name: "Wyoming" },
  ];

  public longName(short:string) {
    let result:string = null;
    let lookup:string = short;

    if (short) {
      lookup = short.toUpperCase();

      for (var i = this.stateInfo.length - 1; i > -1; i--) {
	if (this.stateInfo[i].code2 == lookup) {
	  result = this.stateInfo[i].name;
	  break;
	}
      }
    }

    return result;
  }

  public getStateData(for_state:string) {
    let name = this.longName(for_state) || for_state;
    return this.stateData[name];
  }
}
