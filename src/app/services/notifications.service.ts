// notifications.service.ts: -*- typescript -*-  DESCRIPTIVE TEXT.
// 
//  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
//  Author: Brian J. Fox (bfox@opuslogica.com)
//  Birthdate: Sun Oct  4 02:48:32 2020.
import { Injectable, NgZone } from '@angular/core';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { StorageService } from './storage.service';
import { PushNotification, PushNotificationAPS, APSAlert } from '../interfaces/notifications';
import { ApiService } from './api.service';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})
export class NotificationsService {
  constructor(
    private platform: Platform,
    private readonly firebase: FirebaseX,
    private readonly api: ApiService,
    private nav: NavController,
    private toaster: ToastController,
    private storage: StorageService
  ) { }

  async initializePushTokens() {
    try {
      this.firebase.setAnalyticsCollectionEnabled(false);
      this.firebase.setCrashlyticsCollectionEnabled(false);

      const token = await this.firebase.getToken();
      await this.api.savePushToken(token, 'firebase').toPromise();
    } catch (e) {
      if (e === 'cordova_not_available') {
        console.warn('Skipping native push token init');
      } else {
	alert(`Unable to get or save push token: ${JSON.stringify(e)}`);
      }
    }
  }

  public hasPermission() {
    return this.firebase.hasPermission();
  }

  public getPermission() {
    this.platform.ready().then(async () => {
      this.firebase.hasPermission().then((perm) => {
	if (!perm) {
	  this.firebase.grantPermission().then(async (result) => {
	    if (result) {
	      this.initializePushTokens();
	    } else {
	      alert("Okay.  If you change your mind, update your notification settings in the system settings, then choose your voting state again.");
	    }
	  });
	} else {
	  // alert(`Great news!  We already have permission.`);
	  console.log(`Great news!  We already have permission.`);
	}
      });
    });
  }

  async inform(message:string) {
    let t = await this.toaster.create({ message: message, position: 'top', duration: 5000 });
    t.present(); 
  }

  public initializeNotifications() {
    this.platform.ready().then(() => {
      this.firebase.onTokenRefresh().subscribe((new_token) => {
	this.api.savePushToken(new_token, "firebase").subscribe();
      });
      
      this.firebase.onMessageReceived().subscribe((pn:PushNotification) => {
	if (pn.message) {
	  this.inform(pn.message);
	} else {
	  alert(`onMessageReceived: ${JSON.stringify(pn)} -- look in notifications.service for this.`);
	}
      });
    });
  }
}
