//  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
//  Author: Brian J. Fox (bfox@opuslogica.com)
//  Birthdate: Sat Sep 26 09:21:52 2020.
import { Injectable } from '@angular/core';

function accessor(key: string): PropertyDecorator {
  return <T>(target: any, prop: string | symbol) => {
    Object.defineProperty(target, prop, {
      get(): T {
        return target.getv(key);
      },
      set(val: T): void {
        target.setv(key, val);
      },
    });
  };
}

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  /* Watch out!.  The timing of initialization causes this to be undefined if
     storage.api_server = "foo" is the trigger that loads this class.  I don't
     know enough about the typescript "rules" to resolve this now, hence read
     the horribleness in this.munge(s:string) below. */
  public prefix:string = "mvc";

  @accessor('api_server')
  public api_server: string | undefined;

  @accessor('deviceID')
  public deviceID: string | undefined;

  @accessor('route')
  public route: string | undefined;

  @accessor('welcomed')
  public welcomed: boolean | false;

  @accessor('voting_state')
  public voting_state: string | undefined;

  @accessor('push_token')
  public push_token: string | undefined;

  @accessor('push_token_type')
  public push_token_type: string | undefined;

  @accessor('is_developer')
  public is_developer: boolean | false;

  munge(s:string): string {
    /* Workaround for my lack of loading understanding. */
    if (!this.prefix) this.prefix = "mvc";
    
    return this.prefix + ":" + s.replace(/[_]/g, "-").toLowerCase();
  }
  
  // @ts-ignore
  setv<T = unknown>(name: string, value: T | undefined): void {
    let sname = this.munge(name);
    if (value !== undefined) {
      window.localStorage.setItem(sname, JSON.stringify(value));
    } else {
      this.del(sname);
    }
  }

  // @ts-ignore
  getv<T = unknown>(name: string): T | undefined {
    let sname = this.munge(name);
    const str = window.localStorage.getItem(sname);

    if (str !== null) {
      try {
        return JSON.parse(str);
      } catch (err) {
        console.error(`ERROR PARSING VALUE FOR ${sname}`, err);
      }
    }

    return undefined;
  }

  del(name: string): void {
    let sname = this.munge(name);
    try {
      window.localStorage.removeItem(sname);
    } catch (e) {}
  }

  clearData() {
    window.localStorage.clear();
  }
}
