import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VotedPage } from './voted.page';

const routes: Routes = [
  {
    path: '',
    component: VotedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VotedPageRoutingModule {}
