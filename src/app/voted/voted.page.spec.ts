import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VotedPage } from './voted.page';

describe('VotedPage', () => {
  let component: VotedPage;
  let fixture: ComponentFixture<VotedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VotedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VotedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
