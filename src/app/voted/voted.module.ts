import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../modules/shared.module';
import { VotedPageRoutingModule } from './voted-routing.module';
import { VotedPage } from './voted.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    VotedPageRoutingModule
  ],
  declarations: [VotedPage],
})
export class VotedPageModule {}
