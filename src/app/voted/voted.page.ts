import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { NavController, AlertController, IonSlides } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-voted',
  templateUrl: './voted.page.html',
  styleUrls: ['./voted.page.scss'],
})

export class VotedPage implements OnInit {
  @ViewChild('slides', { static: false })
  private readonly slides!: IonSlides;

  @ViewChild('textarea') textarea: ElementRef;

  public voting_method:any = { by_mail: false, in_person: false };
  public more_details:string = "";
  public shared:boolean = false;
  
  public readonly slideOpts: Readonly<any> = { touchRatio: 0 };

  constructor(
    private http:HttpClient,
    private router:Router,
    private nav:NavController,
    public api:ApiService,
    private social:SocialSharing,
    public alertController:AlertController
  ) { }

  ngOnInit() { }

  ionKeyboardDidShow(event) {
    let input = this.textarea.nativeElement;
    const { keyboardHeight } = event["detail"];
    input.style.setProperty('transform', `translate3d(0, ${keyboardHeight}px, 0)`);
  }

  ionKeyboardDidHide(event=null) {
    let input = this.textarea.nativeElement;
    input.style.removeProperty('transform');
  }
  
  add_listeners() {
    window.addEventListener('ionKeyboardDidShow', this.ionKeyboardDidShow, { once: true });
    window.addEventListener('ionKeyboardDidHide', this.ionKeyboardDidHide, { once: true });
  }

  remove_listeners() {
    window.removeEventListener('ionKeyboardDidShow', this.ionKeyboardDidShow);
    window.removeEventListener('ionKeyboardDidHide', this.ionKeyboardDidHide);
  }
  
  ionViewDidEnter() { this.add_listeners(); }
  ionViewDidLeave() { this.remove_listeners(); }

  updateVotingMethod(event) {
    let cb = event.target;
    if (this.voting_method.by_mail && this.voting_method.in_person) {
      requestAnimationFrame(_ => {
        for (const key of ['by_mail', 'in_person']) {
          this.voting_method[key] = key === cb.name;
        }
      });
    }
  }

  next() { this.slides.slideNext(); }
  prev() { this.slides.slidePrev(); }
  go_home() { this.nav.navigateBack(["/home"]); }

  private method() {
    return this.voting_method.by_mail ? "by mail" : "in person";
  }

  do_share() { this.shared = true; this.show_share(); }
  
  show_share() {
    // this is the complete list of currently supported params you can pass to the plugin (all optional)
    let short_message = "I voted!"
    var options = {
      message: short_message,		 // not supported on some apps (Facebook, Instagram)
      subject: short_message,		 // fi. for email
      // files: ['', ''],		 // an array of filenames either locally or remotely
      url: 'https://myvotecounts.com/voted.html',
      chooserTitle: 'Pick an app'	 // Android only, you can override the default share sheet title
    };
    this.social.shareWithOptions(options);
  }

  finish() {
    let experience = { outcome: "voted", method: this.method(), description: this.more_details };
    this.api.updateExperience(experience).subscribe((response) => { console.log(response); });
    this.slides.slideNext();
  }
}
