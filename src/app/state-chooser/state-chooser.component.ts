import { Component, OnInit } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { StateDataService } from '../services/state-data.service';

@Component({
  selector: 'app-state-chooser',
  templateUrl: './state-chooser.component.html',
  styleUrls: ['./state-chooser.component.scss'],
})

export class StateChooserComponent implements OnInit {

  selected:string = null;
  stateInfo = this.statedata.stateInfo;
  
  constructor(
    private storage: StorageService,
    private statedata: StateDataService
  ) { }

  ngOnInit() { this.selected = this.storage.voting_state; }

  select_this(info) {
    if (info.code2 == this.selected) {
      this.selected = null;
    } else {
      this.selected = info.code2;
    }

    this.update_state(this.selected);
  }

  update_state(to_state) {
    this.storage.voting_state = to_state;
    this.selected = to_state;
  }
}
