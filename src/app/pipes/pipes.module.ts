// pipes.module.ts: -*- typescript -*-  DESCRIPTIVE TEXT.
// 
//  Copyright (c) 2020 Brian J. Fox Opus Logica, Inc.
//  Author: Brian J. Fox (bfox@opuslogica.com)
//  Birthdate: Sat Sep 26 10:15:35 2020.
import { NgModule } from '@angular/core';
import { SafePipe } from './safe.pipe';

@NgModule({
  declarations: [SafePipe],
  imports: [],
  exports: [SafePipe],
})
export class PipesModule {}
