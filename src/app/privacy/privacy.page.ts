import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { StorageService } from '../services/storage.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.page.html',
  styleUrls: ['./privacy.page.scss'],
})
export class PrivacyPage implements OnInit {

  constructor(
    private nav: NavController,
    private alertController: AlertController,
    private api: ApiService,
    private storage: StorageService,
    private iab: InAppBrowser
  ) { }

  browser:any;


  ngOnInit() {
  }

  open_gitlab() {
    this.browser = this.iab.create("https://gitlab.com/osesystems", '_system', ''); 
  }

  async clear_data() {
    const alert = await this.alertController.create({
      header: "Delete Data?",
      message: "Are you sure you want to remove all data? Anything you've given us is anonymous, and will help others to vote.",
      buttons: [
        {text: "Yes",handler: () => {
	  this.api.clearData();
	  this.storage.clearData();
	  alert.dismiss();
	  this.nav.navigateRoot(["/welcome"]);
	}},
	{text: "No", handler: () => { alert.dismiss()}}
      ]
    });
    await alert.present();
  }
}
