#!/bin/sh

if [ ! -e "myvotecounts.app.keystore" ]; then
    echo "Keystore file myvotecounts.app.keystore is not in directory."
    echo "I will attempt to get the android-artifacts git repo in order to find it."
    ( cd ../;
      if [ ! -d android-artifacts ]; then git clone git@git.opuslogica.com:android-artifacts; fi
    )
    cp ../android-artifacts/myvotecounts.app.keystore .

    if [ ! -e "myvotecounts.app.keystore" ]; then
	echo "But I failed!  You'll have to figure this out manually :-("
	exit 1
    else
	echo "Got it - continuing build."
    fi
fi

# First build the 32-bit APK

# But even more first, check to see if we need to rebuild it if there.
unsigned_apk=$(find platforms/android/ -name '*release*.apk')
if [ "$apk" ]; then
    if $(find src -newer "$apk"); then
	ionic cordova build android --release
    fi
fi

# Next build the 64-bit APKs (arm64 & x86_64)
# ionic cordova build android --release -- --xwalk64bit
ionic cordova build android --release

unsigned_apk=$(find platforms/android/ -name '*release*.apk')

if [ "$unsigned_apk" != "" -a -f "$unsigned_apk" ]; then
    cp "$unsigned_apk" ./myvotecounts-release-unsigned.apk

    if [ -r ./myvotecounts.password ]; then
        signing_pass=$(cat ./myvotecounts.password)
    elif [ -r ./android-artifacts/myvotecounts.password ]; then
        signing_pass=$(cat ./android-artifacts/myvotecounts.password)
    elif [ -r ../android-artifacts/myvotecounts.password ]; then
        signing_pass=$(cat ../android-artifacts/myvotecounts.password)
    else
        echo "Couldn't find the file containing the signing password.  Intelligently guessing..."
        signing_pass="secret-password!"
    fi
    export signing_pass

    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ./myvotecounts.app.keystore -storepass:env signing_pass myvotecounts-release-unsigned.apk myvotecounts

    zipalign -f 4 myvotecounts-release-unsigned.apk myvotecounts-release.apk

    rm myvotecounts-release-unsigned.apk

    echo "Your new store-ready apk is named 'myvotecounts-release.apk'"

    chmod a+r myvotecounts-release.apk
    scp myvotecounts-release.apk app.myvotecounts.app:/www/sites/downloads/myvotecounts.apk
fi
